var express = require('express');
var app = express();

// The "non-API Router is likely to exist in a separate file
var nonAPIrouter = express.Router();

// More routes unrelated to APIs
//nonAPIRouter.get();

//////////////////////////////////////////

// The API router may be in it's own file, too.
var APIrouter = express.Router();

// Routes that start with /API
APIrouter.get('/api*', function (req, res) {
  res.send('Hello API!');
});
///////////////////////////////////////


// The main app includes both routers.
app.use(APIrouter);
app.use(nonAPIrouter);

// Default route for requests not matched above
app.use(function(req, res) {
    res.status(404).end('error');
});



app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
