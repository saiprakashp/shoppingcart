const url = require("url");

var getMehtodsFromUrl = function (req) {
  let q = req.originalUrl.replace("/node", "").split("/").splice(2);
  return q;
};

const getUsers = (req, res) => {
  result = {
    userName: "Test multiltenant app2",
    urlUsed: req.originalUrl,
    method:req.method,
    queryParams: req.queryParams,
    body: req.body,
    params: req.params,
    subUrl: getMehtodsFromUrl(req),
    workingUrl: [
      "/getUsers?uid=1",
      "/deleteUser?uid=1",
      "/updateUser",
      "/putUser?uid=1",
    ],
  };
  return result;
};
const deleteUser = (req, res) => {
  result = {
    userName: "Test deleteUser app2",
    method:req.method,
    urlUsed: req.originalUrl,
    queryParams: req.queryParams,
    body: req.body,
    params: req.params,
    subUrl: getMehtodsFromUrl(req),
    workingUrl: [
      "/getUsers?uid=1",
      "/deleteUser?uid=1",
      "/updateUser",
      "/putUser?uid=1",
    ],
  };
  return result;
};
const updateUser = (req, res) => {
  result = {
    userName: "Test updateUser app2",
    method:req.method,
    urlUsed: req.originalUrl,
    queryParams: req.queryParams,
    body: req.body,
    params: req.params,
    subUrl: getMehtodsFromUrl(req),
    workingUrl: [
      "/getUsers?uid=1",
      "/deleteUser?uid=1",
      "/updateUser",
      "/putUser?uid=1",
    ],
  };
  return result;
};
const putUser = (req, res) => {
  result = {
    userName: "Test putUser app2",
    method:req.method,
    urlUsed: req.originalUrl,
    queryParams: req.queryParams,
    body: req.body,
    params: req.params,
    subUrl: getMehtodsFromUrl(req),
    workingUrl: [
      "/getUsers?uid=1",
      "/deleteUser?uid=1",
      "/updateUser",
      "/putUser?uid=1",
    ],
  };
  return result;
};

exports.init = function (req, res, next) {
  req.queryParams = url.parse(req.url, true).query;

  const result = {
    status: "app1 is up and running",
    userName: "From app2",
    urlUsed: req.originalUrl,
    queryParams: req.queryParams,
    body: req.body,
    params: req.params,
    subUrl: getMehtodsFromUrl(req),
    workingUrl: ["/getUsers?uid=1", "/saveUsers"],
  };

  if (req.method === "GET") {
    if (req.originalUrl.search("getUsers") >= 0) {
      res.json(getUsers(req, res));
    } else {
      res.json(result);
    }
  } else if (req.method === "POST") {
    if (req.originalUrl.search("updateUser") >= 0) {
      res.json(updateUser(req, res));
    } else {
      res.json(result);
    }
  } else if (req.method === "PUT") {
    if (req.originalUrl.search("putUser") >= 0) {
      res.json(putUser(req, res));
    } else {
      res.json(result);
    }
  } else if (req.method === "DELETE") {
    if (req.originalUrl.search("deleteUser") >= 0) {
      res.json(deleteUser(req, res));
    } else {
      res.json(result);
    }
  }

  next();
};
