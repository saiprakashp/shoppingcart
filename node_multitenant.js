const e = require('express');

const express = require('express'),
    PORT = 12243,
    FILE_UPLOAD_PATH = "./files/uploads";

var app = express(),
    fs = require('fs'),
    routes = require('./src/routes'),
    fs = require('fs'),
    routes_file_name = 'index';

const replaceFile = () => {
    console.log(`Started replaceFile() ${bareFileName} ${routes_file_name} `)
    fs.readFile('./src/routes.js_tmp', 'utf8', function (err, data) {
        if (err) {
            console.log(err);
        }
        if (data.search(bareFileName) < 0) {
            var result = data.replace("//ADD", `const ${bareFileName} = require('${FILE_UPLOAD_PATH}/${bareFileName}/${routes_file_name}');` + "\n" + "//ADD").replace("//ROUTES", `app.get('/node/${bareFileName}', ${bareFileName}.init); \n //ROUTES`);

            fs.writeFile('./src/routes.js_tmp', result, 'utf8', function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //  restartNode();
                }
            });
        }

    });
}

app.use('/app', routes);

const directories = source => fs.readdirSync(source, {
    withFileTypes: true
}).reduce((a, c) => {
    c.isDirectory() && a.push(c.name)
    return a
}, [])

function loadRoutes() {
    console.log("Started Routes Config based on dir")
    fs.unlink('./src/routes.js', function (err) {
        if (err && err.code == 'ENOENT') {
            console.info("File doesn't exist, won't remove it.");
        } else if (err) {
            console.error("Error occurred while trying to remove file");
        } else {
            console.info(`removed`);
        }
    });
    let dirNames = directories(FILE_UPLOAD_PATH);
    dirNames.map(dirName => {
        bareFileName = dirName;
        replaceFile()
    })

    fs.createReadStream('./src/routes.js_tmp').pipe(fs.createWriteStream('./src/routes.js'));

}

var args = process.argv.slice(2);
try {
    if (args.length > 0 && args[0] === 'replace_routes') {
        console.log('Started Adding Routes')
        loadRoutes()
    } else {
        app.listen(PORT)
        console.log(`Server started on port ${PORT}`);
    }


} catch (error) {
    console.log("Some thing wen wrong !!!  ", error)
}
